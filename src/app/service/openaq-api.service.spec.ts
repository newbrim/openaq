/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { OpenaqApiService } from './openaq-api.service';

describe('OpenaqApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenaqApiService]
    });
  });

  it('should ...', inject([OpenaqApiService], (service: OpenaqApiService) => {
    expect(service).toBeTruthy();
  }));
});
