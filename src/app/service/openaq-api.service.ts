import { Injectable } from '@angular/core';
import { Http, Response }       from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Country }     from '../model/country'
import { City }        from '../model/city'
import { Location }    from '../model/location'
import { Measurement } from '../model/measurement'

@Injectable()
export class OpenaqApiService {

  baseUrl = 'https://api.openaq.org/v1/';

  constructor(private _http: Http) { }

  getCountryList() : Observable <Country[]>{
      return this._http.get(this.baseUrl + 'countries')
          .map( response => response.json().results )
      ;
  }

  getCityList(country) : Observable <City[]>{
      return this._http.get( this.baseUrl + 'cities?country=' + _e( country ) )
          .map( response => response.json().results )
      ;
  }

  getLocationList(country, city) : Observable <Location[]>{
    return this._http.get( this.baseUrl+'locations?country=' + _e( country )+ '&city=' + _e( city ) )
        .map( response => response.json().results )
        ;
  }

  getMeasurementList(country, city, location='', parameter='') : Observable <Measurement[]>{

    var url = this.baseUrl+'measurements?country=' + _e( country ) + '&city=' + _e( city ) + '&parameter=' + parameter;

    // if location is present and empty in uri the api will
    // return 0 results despite matching country/city/param
    // only append location if it has a value
    if(location)
        url += '&location=' + _e( location );

    console.log( url );

    return this._http.get( url )
        .map( response => response.json().results )
        ;
  }

  /*
   * https://api.openaq.org/v1/parameter
   * hardcoding these 7 parameters as not likely to change or have new ones added
   */
  getParameterList() {

    return [
      {
        "id":"pm25",
        "name":"PM2.5",
        "description":"Particulate matter less than 2.5 micrometers in diameter",
        "preferredUnit":"µg/m³"
      },
      {
        "id":"pm10",
        "name":"PM10",
        "description":"Particulate matter less than 10 micrometers in diameter",
        "preferredUnit":"µg/m³"
      },
      {
        "id":"no2",
        "name":"NO2",
        "description":"Nitrogen Dioxide",
        "preferredUnit":"ppm"
      },
      {
        "id":"so2",
        "name":"SO2",
        "description":"Sulfur Dioxide",
        "preferredUnit":"ppm"
      },
      {
        "id":"o3",
        "name":"O3",
        "description":"Ozone",
        "preferredUnit":"ppm"
      },
      {
        "id":"co",
        "name":"CO",
        "description":"Carbon Monoxide",
        "preferredUnit":"ppm"
      },
      {
        "id":"bc",
        "name":"BC",
        "description":"Black Carbon",
        "preferredUnit":"µg/m³"
      }
    ];
  }
}

/*
 didn't like seeing encodeURIComponent all over the place
 */
function _e(value) {
  return encodeURIComponent( value );
}