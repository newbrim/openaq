export class Location{
    constructor(
        public name        : string,
        public city        : string,
        public country     : string,
        public count       : number,
        public sources     : any,
        public lastupdate  : string,
        public firstupdate : string,
        public parameters  : any,
        public source      : string,
        public coordinates : any
    ){}
}