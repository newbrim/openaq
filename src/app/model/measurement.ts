export class Measurement{
    constructor(
        public country     : string,
        public city        : string,
        public location    : string,
        public parameter   : any,
        public date        : string,
        public local       : string,
        public value       : number,
        public unit        : string,
        public coordinates : any
    ){}
}