export class Country {
    constructor(
        public name     : string,
        public code     : string,
        public cities   : number,
        public locations: number,
        public count    : number
    ){}
}