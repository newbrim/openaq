export class City {
    constructor(
        public name     : string,
        public country  : string,
        public locations: number,
        public count    : number
    ){}
}