import { Component, OnInit } from '@angular/core';
import { OpenaqApiService } from './service/openaq-api.service'

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title = 'Open<span>A</span>q';
  subtitle = 'Open, Real-time Air Quality Data';


  countries;
  cities;
  locations;
  parameters;
  measurements;
  history = [];

  selectedCountry;
  selectedCity;
  selectedLocation;
  selectedParameter;

  constructor( private _openaqApiService : OpenaqApiService ) {}

  ngOnInit() {
      this.getCountries();
      this.getParameters();
  }

  getCountries() {
    this._openaqApiService.getCountryList()
        .subscribe(countries => this.countries = countries);
  }

  getCities() {
    this._openaqApiService.getCityList(this.selectedCountry)
        .subscribe(cities => this.cities = cities);
  }

  getLocations() {
    this._openaqApiService.getLocationList(this.selectedCountry, this.selectedCity)
        .subscribe(locations => this.locations = locations);
  }

  getParameters() {
    this.parameters = this._openaqApiService.getParameterList();
  }

  getMeasurements(form) {
    this._openaqApiService.getMeasurementList(this.selectedCountry, this.selectedCity, this.selectedLocation, this.selectedParameter)
        .subscribe(measurements => {
          this.measurements = measurements;
          this.history.push( measurements );
        });
  }

  changeCountry(country) {
    this.selectedCountry = country;
    this.getCities();
  }

  changeCity(city) {
    console.log('encodeURIComponent', encodeURIComponent(city) );
    console.log('encode', encodeURI(city) );

    this.selectedCity = city;
    this.getLocations();
  }

  changeLocation(location) {
    this.selectedLocation = location;
  }

  changeParameter(parameter) {
    this.selectedParameter = parameter;
  }
}